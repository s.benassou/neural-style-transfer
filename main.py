import os
import time
import functools
import tensorflow as tf
import tensorflow_hub as hub

import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np
import PIL.Image

from flask import Flask, jsonify, render_template, request

from img_processing import load_img, tensor_to_image

# Load compressed models from tensorflow_hub
os.environ['TFHUB_MODEL_LOAD_FORMAT'] = 'COMPRESSED'

mpl.rcParams['figure.figsize'] = (12, 12)
mpl.rcParams['axes.grid'] = False

PEOPLE_FOLDER = os.path.join('static', 'people_photo')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = PEOPLE_FOLDER

@app.route("/")
def style():
    img_to_style = "static/data/beauty-wandel-johnny-depp-2017-05-18.jpg"
    #content_path = tf.keras.utils.get_file('YellowLabradorLooking_new.jpg', 'https://storage.googleapis.com/download.tensorflow.org/example_images/YellowLabradorLooking_new.jpg')
    style_path = tf.keras.utils.get_file('kandinsky5.jpg','https://storage.googleapis.com/download.tensorflow.org/example_images/Vassily_Kandinsky%2C_1913_-_Composition_7.jpg')
    content_image = load_img(img_to_style)
  
    style_image = load_img(style_path) 
    style_name = tensor_to_image(style_image, "static/styles/out.jpg") 
   # tensor_to_image(stylized_image)
    hub_model = hub.load('https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/2')
    stylized_image = hub_model(tf.constant(content_image), tf.constant(style_image))[0]
    name = tensor_to_image(stylized_image, "static/people_photo/out.jpg") 
   # name = os.path.join(app.config['UPLOAD_FOLDER'], name)
    return render_template("welcome.html", user_image = [img_to_style, style_name, name])


if __name__ == '__main__': 
    app.run(port=12346, debug=True)